import { IFileUpload } from '../../interfaces/file.interface';
import * as OSS from 'ali-oss';
import { readFileSync } from 'fs';
import { OssConfig } from './oss.config';

export class OssFileUploadService implements IFileUpload {
  client;
  constructor(private readonly config: OssConfig) {
    this.client = new OSS({
      accessKeyId: this.config.accessKeyId,
      accessKeySecret: this.config.secretAccessKey,
      bucket: this.config.bucket,
      region: this.config.region,
    });
  }

  upload(destPath: string, sourcePath: string | Buffer) {
    return this.client.put(
      destPath,
      Buffer.isBuffer(sourcePath) ? sourcePath : readFileSync(sourcePath),
    );
  }

  getBucket(): string {
    return this.config.bucket;
  }

  getProviderType(): string {
    return 'oss';
  }
}
