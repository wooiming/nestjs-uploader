import { Controller, Get, Post, UploadedFile, Inject, UseInterceptors } from '@nestjs/common';
import { AppService } from './app.service';
import { FileUploadModule } from './file-upload/file-upload.module';
import { IFileUpload } from './file-upload/interfaces/file.interface';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('FileUploadService') private readonly fileUploadService: IFileUpload,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/upload')
  @UseInterceptors(FileInterceptor('photo'))
  async create(@UploadedFile() file) {
    // console.log(file)
    const results = await this.fileUploadService.upload('test/testing.jpg', file.path);
    console.log(results);
    return 'ok';
    // file.path
  }
}
