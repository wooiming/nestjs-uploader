import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileUploadModule } from './file-upload/file-upload.module';
import { FileUploadType } from './file-upload/file-upload.enum';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as path from 'path';

@Module({
  imports: [
    MulterModule.registerAsync({
      useFactory: () => ({
        storage: diskStorage({
          destination: path.dirname(path.dirname(__dirname)) + '/upload',
          fileFilter: (req, file, cb) => {
            if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
              return cb(new Error('Only image files are allowed!'), false);
            }
            cb(null, true);
          },
          filename: (req, file, cb) => {
            const fileExtName = path.extname(file.originalname);
            cb(null, `${fileExtName}`);
          },
        }),
      }),
    }),
    // register the file upload module
    // config available
    /**
     * S3Config : './file-upload/uploader/s3/s3.config';
     * OssConfig : './file-upload/uploader/oss/oss.config';
     */
    FileUploadModule.register({
      type: FileUploadType.S3, // options: FileUploadType.S3 / FileUploadType.OSS
      config: {
        bucketName: 'bucketName', // 
        accessKeyId: 'accessKeyId',
        secretAccessKey: 'secretAccessKey',
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
