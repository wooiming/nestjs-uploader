export interface OssConfig {
  accessKeyId: string;
  accessKeySecret: string;
  secretAccessKey: string;
  bucket: string;
  region: string;
}
