import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { IFileUpload } from '../../interfaces/file.interface';
import { readFileSync } from 'fs';
import { lookup } from 'mime-types';
import { S3Config } from './s3.config';

@Injectable()
export class S3FileUploadService implements IFileUpload {
  aws: typeof AWS;
  s3: AWS.S3;

  constructor(private readonly config: S3Config) {
    this.aws = AWS;
    const { apiVersion, accessKeyId, secretAccessKey } = this.config;

    this.s3 = new AWS.S3({
      apiVersion: apiVersion || '2006-03-01',
      accessKeyId,
      secretAccessKey,
    });
  }

  async upload(destPath: string, sourcePath: string | Buffer): Promise<any> {
    const params = {
      Bucket: this.getBucket(),
      Key: destPath,
      Body: Buffer.isBuffer(sourcePath) ? sourcePath : readFileSync(sourcePath),
      ACL: 'public-read',
      ContentType: lookup(destPath),
    };

    return new Promise((resolve, reject) => {
      this.s3.putObject(params, (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
      });
    });
  }

  getBucket(): string {
    return this.config.bucketName;
  }

  getProviderType(): string {
    return 's3';
  }
}
