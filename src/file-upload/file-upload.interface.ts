export interface IFileUploadService {
  upload(destPath: string, sourcePath: string);
}
