export interface S3Config {
  apiVersion?: string;
  accessKeyId: string;
  secretAccessKey: string;
  bucketName: string;
}
