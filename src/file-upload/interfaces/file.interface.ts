export interface IFile {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  buffer: any;
  size: number;
}

export interface IFileUpload {
  upload(destPath: string, sourcePath: string);
  getBucket(): string;
  getProviderType(): string;
}
