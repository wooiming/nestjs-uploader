import { FileUploadType } from './file-upload.enum';
import { S3Config } from './uploader/s3/s3.config';
import { OssConfig } from './uploader/oss/oss.config';

export interface FileUploadOption {
  type: FileUploadType;
  config: S3Config | OssConfig;
}
