import { Module, DynamicModule } from '@nestjs/common';
import { S3FileUploadService } from './uploader/s3/s3-file-upload.service';
import { OssFileUploadService } from './uploader/oss/oss-file-upload.service';
import { FileUploadOption } from './file-upload.option';
import { FileUploadType } from './file-upload.enum';
import { S3Config } from './uploader/s3/s3.config';
import { OssConfig } from './uploader/oss/oss.config';

const uploadProvider = (option: FileUploadOption) => ({
  provide: 'FileUploadService',
  useFactory: () => {
    switch (option.type) {
      case FileUploadType.S3:
        return new S3FileUploadService(option.config as S3Config);
      case FileUploadType.OSS:
        return new OssFileUploadService(option.config as OssConfig);
    }
  },
});

@Module({})
export class FileUploadModule {
  static register(option: FileUploadOption): DynamicModule {
    const provider = uploadProvider(option);
    return {
      module: FileUploadModule,
      providers: [provider],
      exports: [provider],
    };
  }
}
